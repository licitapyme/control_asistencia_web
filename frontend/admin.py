# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.admin import SimpleListFilter

from django.utils import timezone

from import_export.resources import ModelResource
from import_export.admin import ExportMixin
from import_export.fields import Field

from rangefilter.filter import DateRangeFilter

from frontend.models import *

# Register your models here.
admin.site.register(Compania)

class BomberoVoluntarioResource(ModelResource):

    compania = Field(attribute='compania', column_name='CIA')
    operador_rescate = Field(attribute='get_operador_rescate_fmt', column_name='Operador')
    conductor = Field(attribute='get_conductor_fmt', column_name='Conductor')
    tiene_foto = Field(attribute='has_picture_fmt', column_name='Tiene Foto')

    class Meta:
        model = BomberoVoluntario
        fields = ('rut', 'nombre_completo', 'compania', 'operador_rescate', 
            'conductor', 'status', 'created',)
        export_order = fields


@admin.register(BomberoVoluntario)
class BomberoVoluntarioAdmin(ExportMixin, admin.ModelAdmin):
    resource_class = BomberoVoluntarioResource

    list_per_page = 15

    list_display = (
        'rut',
        'nombre_completo',
        'compania',
        'cargo',
        'operador_rescate',
        'conductor',
        'has_picture_fmt',
        'status',
        'created_fmt',
    )
    search_fields = ['^rut', 'nombre_completo']
    list_filter = ['operador_rescate', 'status', 'compania']
    list_editable = ['status']
    readonly_fields = ('image_tag', 'internal_id',)
    exclude = ('internal_id', 'status',)

    def created_fmt(self, obj):
        to_zone = timezone.get_default_timezone()
        created = obj.created.astimezone(to_zone)
        return created.strftime("%d/%m/%Y %H:%M")
    created_fmt.admin_order_field = 'created'
    created_fmt.short_description = 'Creado'


class AsistenciaDetalleResource(ModelResource):

    nombre_completo = Field(attribute='get_nombre_completo', column_name='Nombre Completo')
    rut = Field(attribute='get_rut', column_name='RUT')
    compania = Field(attribute='get_ccia', column_name='CIA')
    operador_rescate = Field(attribute='get_operador_rescate', column_name='Operador')
    conductor = Field(attribute='get_conductor', column_name='Conductor')
    fecha_hora_ingreso = Field(attribute='get_fecha_hora_ingreso', column_name='Fecha/Hora Ingreso')
    fecha_hora_salida = Field(attribute='get_fecha_hora_salida', column_name='Fecha/Hora Salida')

    class Meta:
        model = AsistenciaDetalle
        fields = ('nombre_completo', 'rut', 'compania', 'operador_rescate', 
            'fecha_hora_ingreso', 'temperatura_ingreso', 
            'fecha_hora_salida', 'temperatura_salida',)
        export_order = fields

# 
class PresenteFilter(SimpleListFilter):
    title = 'Presente?'
    parameter_name = 'presente'

    def lookups(self, request, model_admin):
        return [('si', 'Si'), ('no', 'No')]

    def queryset(self, request, queryset):
        if self.value() == 'si':
            return queryset.filter(fecha_hora_salida__isnull=True)
        if self.value() == 'no':
            return queryset.filter(fecha_hora_salida__isnull=False)
        else:
            return queryset.all()

@admin.register(AsistenciaDetalle)
class AsistenciaDetalleAdmin(ExportMixin, admin.ModelAdmin):
    resource_class = AsistenciaDetalleResource

    list_per_page = 15

    list_display = (
        'personal',
        'get_rut',
        'get_ccia',
        'get_operador_rescate',
        'get_conductor',
        'fecha_hora_ingreso_fmt',
        'temperatura_ingreso',
        'fecha_hora_salida_fmt',
        'temperatura_salida',
    )
    search_fields = ['personal__nombre_completo']
    list_filter = (
        ('fecha_hora_ingreso', DateRangeFilter),
        PresenteFilter, 
        'personal__operador_rescate', 
        'compania', 
        )
    readonly_fields = ('personal',)

    def fecha_hora_ingreso_fmt(self, obj):
        to_zone = timezone.get_default_timezone()
        dt = obj.fecha_hora_ingreso.astimezone(to_zone)
        return dt.strftime("%d/%m/%Y %H:%M:%S")
    fecha_hora_ingreso_fmt.admin_order_field = 'fecha_hora_ingreso'
    fecha_hora_ingreso_fmt.short_description = 'Fecha/Hora Ingreso'

    def fecha_hora_salida_fmt(self, obj):
        if obj.fecha_hora_salida:
            to_zone = timezone.get_default_timezone()
            dt = obj.fecha_hora_salida.astimezone(to_zone)
            return dt.strftime("%d/%m/%Y %H:%M:%S")
        else:
            return '-'
    fecha_hora_salida_fmt.admin_order_field = 'fecha_hora_salida'
    fecha_hora_salida_fmt.short_description = 'Fecha/Hora Salida'

    def has_add_permission(self, request):
        # Nobody is allowed to add
        return False
    # def has_delete_permission(self, request, obj=None):
    #     # Nobody is allowed to delete
    #     return False


# Text to put in each page's <h1> (and above login form). By default: Django administration.
admin.site.site_header = 'Cuerpo de Bomberos Peñaflor: Control de Asistencia (Admin)'
# Text to put at the end of each page's <title>. By default: Django site admin.
admin.site.site_title = 'Control de Asistencia (Admin)'
# Text to put at the top of the admin index page. By default: Site administration.
admin.site.index_title = 'Cuerpo de Bomberos Peñaflor: Control de Asistencia: Administración'
#
# admin.site.site_url = None
#
admin.site.content_title = 'ed'

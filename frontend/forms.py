
from django import forms

from frontend.models import Compania


class ImageForm(forms.Form):
    """ Ajax Form to submit image bytes """

    capture_type = forms.CharField(widget=forms.HiddenInput(), required=True, initial='')

    def __init__(self, *args, **kwargs):
        # 
        capture_type = kwargs.pop('capture_type', 'IN')

        super(ImageForm, self).__init__(*args, **kwargs)

        self.fields['capture_type'].initial = capture_type


class IngresoRutForm(forms.Form):
    """ Ingreso de RUT """

    codigo_rut = forms.CharField(widget = forms.TextInput(attrs={'class':'form-control input-sm', 'placeholder': 'RUT', 'autofocus': ''}), 
        required=True, max_length=25, initial='')

    codigo_documento = forms.CharField(widget = forms.TextInput(attrs={'class':'form-control input-sm', 'placeholder': 'Código Documento'}), 
        required=True, max_length=25, initial='')

    compania = forms.ModelChoiceField(
        empty_label='Compañia (Opcional)',
        queryset=None, widget=forms.Select(attrs={'class':'form-control input-sm'}), required=False)

    capture_type = forms.CharField(widget=forms.HiddenInput(), required=True, initial='')

    def __init__(self, *args, **kwargs):
        # 
        capture_type = kwargs.pop('capture_type', 'IN')

        super(IngresoRutForm, self).__init__(*args, **kwargs)

        self.fields['capture_type'].initial = capture_type
        # 
        self.fields['compania'].queryset = Compania.objects.all()


class IngresoTemperaturaForm(forms.Form):
    """ Ingreso de Temperatura """

    temperatura = forms.DecimalField(max_digits=4, decimal_places=2, initial=36.5)
    capture_type = forms.CharField(widget=forms.HiddenInput(), required=True, initial='')

    def __init__(self, *args, **kwargs):
        super(IngresoTemperaturaForm, self).__init__(*args, **kwargs)

        self.fields['temperatura'].widget = forms.NumberInput(attrs={
            'class':'form-control',
            'required':'',
            'autofocus':'',
            'step':'0.1',
        })

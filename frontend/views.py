# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import View, TemplateView
from django.db.models import Q, Count, Max

from django.utils import timezone
from django.conf import settings
from django.http import JsonResponse

from django.shortcuts import (
    get_object_or_404, redirect, render)

from django.contrib.messages import info, warning

from frontend.forms import ImageForm, IngresoRutForm, IngresoTemperaturaForm

from frontend.models import BomberoVoluntario, AsistenciaDetalle, Compania

from backend.tasks import compare_faces_wrapper_task

from tempfile import NamedTemporaryFile

from datetime import timedelta, datetime, time

import os

# Create your views here.

class HomeView(TemplateView):
    template_name = 'frontend/home.html'

    @method_decorator(
        login_required(login_url=reverse_lazy('login')))
    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)

        # Valida permisos de usuario (Operador_Central)
        has_report_permission = True
        user = request.user
        group = user.groups.filter(name='Operador_Central').first()
        has_report_permission = True if group else False
        context['has_report_permission'] = has_report_permission

        # Valida permisos de usuario (Ingreso_Salida)
        has_control_permission = True
        group = user.groups.filter(name='Ingreso_Salida').first()
        has_control_permission = True if group else False
        context['has_control_permission'] = has_control_permission

        # Valida permisos de usuario (Setup)
        has_setup_permission = True
        group = user.groups.filter(name='Setup').first()
        has_setup_permission = True if group else False
        context['has_setup_permission'] = has_setup_permission

        return self.render_to_response(context)


class AttendanceLandingView(TemplateView):
    form_class = IngresoTemperaturaForm
    template_name = 'frontend/attendance_landing.html'

    @method_decorator(
        login_required(login_url=reverse_lazy('login')))
    def get(self, request, *args, **kwargs):
        form = self.form_class()

        if 'temperatura' in request.session:
            del request.session['temperatura']
        if 'conductor' in request.session:
            del request.session['conductor']

        return render(
            request,
            self.template_name, {'form': form}
        )

    def post(self, request, *args, **kwargs):
        bound_form = self.form_class(request.POST)

        if bound_form.is_valid():
            capture_type = bound_form.cleaned_data['capture_type']
            temperatura = bound_form.cleaned_data['temperatura']

            request.session['temperatura'] = str(temperatura)

            next_url = f'/recon_facial/?t={capture_type}'
            return redirect(next_url)
        else:
            msg = "El formato de la temperatura no es válido."
            # shortcut for add_message
            info(
                request,
                msg)

        return render(
            request,
            self.template_name, {'form': bound_form}
        )


class CaptureImageView(View):
    form_class = ImageForm
    template_name = 'frontend/capture_image_home.html'

    @method_decorator(
        login_required(login_url=reverse_lazy('login')))
    def get(self, request, *args, **kwargs):
        context = {'capture_type': 'ingreso'}
        # 
        capture_type = 'IN'
        if 't' in request.GET:
            capture_type = request.GET['t']
            context['capture_type'] = capture_type
            # 
            capture_type = 'IN' if capture_type=='ingreso' else 'OUT'
        form = self.form_class(capture_type=capture_type)

        if 'mensaje_captura' in request.session:
            del request.session['mensaje_captura']

        context['form'] = form

        return render(
            request,
            self.template_name, context
        )

    def post(self, request, *args, **kwargs):
        bound_form = self.form_class(request.POST)
        if request.is_ajax() and bound_form.is_valid():
            capture_type = bound_form.cleaned_data['capture_type']
            filepath = handle_uploaded_file(request.FILES['image'], capture_type)
            # 
            compania = -1
            if 'compania' in request.session:
                compania = request.session['compania']
            # 
            task_kwargs = {
                'target_path': filepath,
                'compania': compania,
            }
            internal_id = compare_faces_wrapper_task(**task_kwargs)

            attendance_type = 'Ingreso' if capture_type=='IN' else 'Salida'
            msg = f'Su {attendance_type} se ha realizado satisfactoriamente.'
            if internal_id < 0:
                msg = f'Su {attendance_type} NO se ha realizado satisfactoriamente.'
            else:
                resource = get_object_or_404(BomberoVoluntario, internal_id=internal_id)

                temperatura = None
                if 'temperatura' in request.session:
                    temperatura = request.session['temperatura']

                fecha_hora_registro = None
                if capture_type=='IN':
                    # Create AsistenciaDetalle: personal, fecha_hora_ingreso
                    fecha_hora_registro = timezone.localtime()
                    attendance_log = AsistenciaDetalle(
                        personal=resource,
                        fecha_hora_ingreso=fecha_hora_registro,
                        temperatura_ingreso=temperatura,
                        compania=resource.compania,
                    )
                    attendance_log.save()
                    # 
                    if resource.conductor:
                        request.session['conductor'] = True
                        request.session['asistencia_id'] = attendance_log.id
                else:
                    # Update AsistenciaDetalle: fecha_hora_salida
                    for attendance_log in resource.asistencia.all()[:5]:
                        if attendance_log.fecha_hora_salida:
                            continue
                        fecha_hora_registro = timezone.localtime()
                        attendance_log.fecha_hora_salida = fecha_hora_registro
                        attendance_log.temperatura_salida = temperatura
                        attendance_log.save()

                if fecha_hora_registro:
                    fecha_captura = fecha_hora_registro.strftime("%d-%m-%Y")
                    hora_captura = fecha_hora_registro.strftime("%H:%M:%S")

                    nombre_completo = resource.nombre_completo
                    nombre_ccia = resource.compania
                    texto_Ingreso = 'ingresado al' if capture_type=='IN' else 'salido del'
                    message = f"Voluntario {nombre_completo} ha {texto_Ingreso} Cuartel {nombre_ccia} el {fecha_captura} a las {hora_captura}."
                    # 
                    request.session['mensaje_captura'] = message

            # shortcut for add_message
            info(
                request,
                msg)

        return render(
            request,
            self.template_name, {'form': bound_form}
        )


class CaptureImageOkView(TemplateView):
    template_name = 'frontend/captura_aceptada_ok.html'

    @method_decorator(
        login_required(login_url=reverse_lazy('login')))
    def get(self, request, *args, **kwargs):
        return super(CaptureImageOkView, self).get(request, *args, **kwargs)


class IngresoRutView(TemplateView):
    form_class = IngresoRutForm
    template_name = 'frontend/ingreso_rut.html'

    @method_decorator(
        login_required(login_url=reverse_lazy('login')))
    def get(self, request, *args, **kwargs):
        capture_type = 'IN'
        if 't' in request.GET:
            capture_type = request.GET['t']
            capture_type = 'IN' if capture_type=='ingreso' else 'OUT'
        form = self.form_class(capture_type=capture_type)

        if 'mensaje_captura' in request.session:
            del request.session['mensaje_captura']

        return self.render_to_response({'form': form})

    def post(self, request, *args, **kwargs):
        bound_form = self.form_class(request.POST)
        if bound_form.is_valid():
            capture_type = bound_form.cleaned_data['capture_type']

            codigo_rut = bound_form.cleaned_data['codigo_rut']
            codigo_documento = bound_form.cleaned_data['codigo_documento']

            resource = BomberoVoluntario.objects.filter(rut=codigo_rut, codigo_documento=codigo_documento).first()
            # 
            internal_id = -1
            if resource:
                internal_id = resource.internal_id

            temperatura = None
            if 'temperatura' in request.session:
                temperatura = request.session['temperatura']

            # Agrega compañía a la cual está ingresando
            compania = bound_form.cleaned_data['compania']
            compania = compania if compania else resource.compania

            attendance_type = 'Ingreso' if capture_type=='IN' else 'Salida'
            msg = f'Su {attendance_type} se ha realizado satisfactoriamente.'
            if internal_id < 0:
                msg = f'Voluntario no registrado, por favor comuníquese con la Secretaria de Comandancia.'
            else:
                fecha_hora_registro = None
                if capture_type=='IN':
                    # Create AsistenciaDetalle: personal, fecha_hora_ingreso
                    fecha_hora_registro = timezone.localtime()
                    attendance_log = AsistenciaDetalle(
                        personal=resource,
                        fecha_hora_ingreso=fecha_hora_registro,
                        temperatura_ingreso=temperatura,
                        modo_ingreso='rut',
                        compania=compania,
                    )
                    attendance_log.save()
                    # 
                    if resource.conductor:
                        request.session['conductor'] = True
                        request.session['asistencia_id'] = attendance_log.id
                else:
                    # Update AsistenciaDetalle: fecha_hora_salida
                    for attendance_log in resource.asistencia.all()[:5]:
                        if attendance_log.fecha_hora_salida:
                            continue
                        fecha_hora_registro = timezone.localtime()
                        attendance_log.fecha_hora_salida = fecha_hora_registro
                        attendance_log.temperatura_salida = temperatura
                        attendance_log.modo_salida = 'rut'
                        attendance_log.save()

                if fecha_hora_registro:
                    fecha_captura = fecha_hora_registro.strftime("%d-%m-%Y")
                    hora_captura = fecha_hora_registro.strftime("%H:%M:%S")

                    nombre_completo = resource.nombre_completo
                    nombre_ccia = compania
                    texto_Ingreso = 'ingresado al' if capture_type=='IN' else 'salido del'
                    message = f"Voluntario {nombre_completo} ha {texto_Ingreso} Cuartel {nombre_ccia} el {fecha_captura} a las {hora_captura}."
                    # 
                    request.session['mensaje_captura'] = message

            # shortcut for add_message
            info(
                request,
                msg)

            return redirect('/captura_aceptada')

        return self.render_to_response({'form': bound_form})


class OperadorReportView(TemplateView):

    template_name = 'frontend/operador_report.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['local_time'] = timezone.localtime()

        localdate = timezone.localdate()
        date_value = datetime.combine(localdate, time.min)
        start_date = localize_date(date_value)

        # Voluntarios deben estar visibles de acuerdo a último ingreso.
        start_date += timedelta(days=-5)

        context['asistencia_resumen'] = get_asistencia_resumen(start_date)
        # 
        context['asistencia_detalle'] = get_asistencia_detalle(start_date)

        return context


def check_password(request):
    password = request.POST.get('password1', '')

    # 
    user = request.user
    pwd_ok = user.check_password(password)

    data = {
        'pwd_ok': pwd_ok,
    }
    return JsonResponse(data)


def set_compania(request):
    compania = request.POST.get('compania', '')

    internal_id = -1
    compania_nombre = ''
    cia_ok = False
    # 
    obj = Compania.objects.filter(nombre=compania).first()
    if obj:
        internal_id = obj.internal_id
        compania_nombre = obj.nombre
        cia_ok = True
    request.session['compania'] = internal_id
    request.session['compania_nombre'] = compania_nombre

    data = {
        'cia_ok': cia_ok,
    }
    return JsonResponse(data)


def set_ingreso_conductor(request):
    """ Si bombero es conductor, detalle Conductor deberá decir que SI solamente si el bombero ingreso como conductor """
    # 
    asistencia_id = request.POST.get('asistencia_id', -1)

    _ok = False
    try:
        obj = AsistenciaDetalle.objects.get(id=asistencia_id)
        obj.conductor = True
        obj.save()
        _ok = True
    except Exception as e:
        pass
    # 
    if 'conductor' in request.session:
        del request.session['conductor']

    data = {
        '_ok': _ok,
    }
    return JsonResponse(data)


def get_asistencia_resumen(start_date):
    # 
    qs = (AsistenciaDetalle.objects
    .values('compania__nombre')
    .filter(fecha_hora_ingreso__gte=start_date, fecha_hora_salida__isnull=True)
    .annotate(
        voluntarios=(
            Count('personal_id', distinct=True)
        ),
        operadores=(
            Count('personal_id', distinct=True, filter=Q(personal__operador_rescate=True))
        ),
        conductores=(
            Count('personal_id', distinct=True, filter=Q(
                conductor=True))
        ),
    )
    .order_by('compania'))

    return qs


def get_asistencia_detalle(start_date):
    # Initial queryset
    qs = AsistenciaDetalle.objects.filter(
        fecha_hora_ingreso__gte=start_date,
        fecha_hora_salida__isnull=True)

    # Get all personal IDs for asistencia
    personal_ids = set(qs.values_list('personal', flat=True))

    # Get latest asistencia for personal IDs
    qs1 = BomberoVoluntario.objects.filter(pk__in=personal_ids).annotate(latest_id=Max('asistencia__id'))
    latest_ids = qs1.values_list('latest_id', flat=True)

    # Filter asistencia
    qs = qs.filter(pk__in=latest_ids)

    return qs


def localize_date(date_value):
    # 
    srv_tz = timezone.get_default_timezone()
    local_date = srv_tz.localize(date_value)

    return local_date


def handle_uploaded_file(f, capture_type='IN'):
    today_folder = timezone.localdate().strftime("%Y-%m-%d")

    # Set full path to today_folder where file will be saved
    path_to_img = os.path.join(settings.MEDIA_ROOT, today_folder)

    # Check if today_folder already exists
    if not os.path.exists(path_to_img):
        os.mkdir(path_to_img)

    capture_type = capture_type.lower()
    kwargs = {
        'prefix': f"{capture_type}_",
        'suffix': '.png',
        'dir': path_to_img,
        'delete': False,
    }

    filepath = ''
    try:
        with NamedTemporaryFile(**kwargs) as destination:
            if f.multiple_chunks:  # size is over than 2.5 Mb
                for chunk in f.chunks():
                    destination.write(chunk)
            else:
                destination.write(f.read())
            filepath = destination.name
            # filepath = f'{today_folder}/{filename}'
    except Exception as e:
        pass

    return filepath
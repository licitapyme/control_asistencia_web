from django.conf.urls import url

from frontend.views import (
    HomeView, CaptureImageView, AttendanceLandingView, 
    CaptureImageOkView, IngresoRutView,
    OperadorReportView,
    check_password, set_compania, set_ingreso_conductor )

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^recon_facial/$', CaptureImageView.as_view(), name='capture_image'),
    url(r'^ingreso_salida/$', AttendanceLandingView.as_view(), name='attendance_landing'),
    url(r'^captura_aceptada/$', CaptureImageOkView.as_view(), name='captura_aceptada'),
    url(r'^ingreso_rut/$', IngresoRutView.as_view(), name='ingreso_rut'),
    url(r'^operador_reporte/$', OperadorReportView.as_view(), name='operador_reporte'),
    url(r'^ajax/check-password/$',
        check_password,
        name='check_password'),
    url(r'^ajax/set-compania/$',
        set_compania,
        name='set_compania'),
    url(r'^ajax/set-ingreso-conductor/$',
        set_ingreso_conductor,
        name='set_ingreso_conductor'),
]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.utils import timezone
from django.utils.timezone import now as timezone_now

import os

# Create your models here.

def upload_to_svr(instance, filename):
    now = timezone_now()
    filename_base, filename_ext = os.path.splitext(filename)
    return "personal_images/{}_{}{}".format(
        instance.rut,
        now.strftime("%Y_%m_%d_%H%M%S"),
        filename_ext.lower(),
    )


class Compania(models.Model):
    """ Compania Bomberos """

    internal_id = models.AutoField(primary_key=True)

    nombre = models.CharField('Nombre Compañía', max_length=150)

    class Meta:
        db_table = 'Compania_Bombero'
        verbose_name = 'Compañía de Bomberos'
        verbose_name_plural = 'Compañías de Bomberos'

    def __str__(self):
        return self.nombre


class BomberoVoluntario(models.Model):
    """ Bombero Voluntario """

    STATUS_CHOICES = (
      ( 'active' , 'Activo'),
      ( 'non-active' , 'No Activo'),
    )

    internal_id = models.AutoField(primary_key=True)

    rut = models.CharField(max_length=60, unique=True)
    codigo_documento = models.CharField('Código Documento', max_length=20)
    nombre_completo = models.CharField('Nombre Completo', max_length=20)

    compania = models.ForeignKey(Compania, related_name='+', verbose_name='Compañía', null=True, on_delete=models.SET_NULL)

    cargo = models.CharField('Cargo', max_length=255, blank=True, default='')
    email = models.CharField('Correo Electrónico', max_length=150, blank=True, default='')
    fono = models.CharField('Teléfono Contacto', max_length=20, blank=True, default='')

    operador_rescate = models.BooleanField('Operador de Rescate', blank=True, 
        default=False)

    conductor = models.BooleanField('Conductor', blank=True, 
        default=False)

    # Foto personal
    personal_foto = models.ImageField('Foto Ruta',
        upload_to=upload_to_svr,
        default='/',
        # blank=True,
        # null=True,
    )

    status = models.CharField('Status', choices=STATUS_CHOICES, 
        max_length=20, blank=True, default='non-active')

    created = models.DateTimeField(
        "Fecha y hora creación",
        auto_now_add=True)

    class Meta:
        db_table = 'Bombero_Voluntario'
        verbose_name = 'Bombero Voluntario'
        verbose_name_plural = 'Bomberos Voluntarios'

    def __str__(self):
        return f"{self.nombre_completo} [{self.rut}]"

    def as_dict(self):
        return {
            'internal_id': self.internal_id,
            'source_path': self.personal_foto.name
        }

    def image_tag(self):
        from django.utils.html import mark_safe
        return mark_safe('<img src="{url}" width={width} height={height} />'.format(
            url = self.personal_foto.url,
            width='auto',
            height='225px',
            )
        )
    image_tag.short_description = 'Foto Voluntario'
    image_tag.allow_tags = True

    def get_operador_rescate_fmt(self):
        return 'SI' if self.operador_rescate else 'NO'

    def get_conductor_fmt(self):
        return 'SI' if self.conductor else 'NO'

    def has_picture(self):
        return False if self.personal_foto.name=='/' else True

    def has_picture_fmt(self):
        return 'SI' if self.has_picture() else 'NO'
    has_picture_fmt.short_description = 'Tiene Foto'


class AsistenciaDetalle(models.Model):
    """ Detalle Asistencia """

    MODO_CHOICES = (
      ( 'facial' , 'Facial'),
      ( 'rut' , 'Rut'),
      ( 'manual' , 'Manual'),
    )

    personal = models.ForeignKey(BomberoVoluntario, related_name='asistencia', verbose_name='Bombero Voluntario', null=True, on_delete=models.CASCADE)

    fecha_hora_ingreso = models.DateTimeField('Fecha/Hora Ingreso', null=True, blank=True)
    temperatura_ingreso = models.DecimalField('Temp. Ing', max_digits=5, decimal_places=2, blank=True, null=True)
    modo_ingreso = models.CharField('Modo Ingreso', choices=MODO_CHOICES, max_length=20, blank=True, default='facial')

    fecha_hora_salida = models.DateTimeField('Fecha/Hora Salida', null=True, blank=True)
    temperatura_salida = models.DecimalField('Temp. Sal', max_digits=5, decimal_places=2, blank=True, null=True)
    modo_salida = models.CharField('Modo Salida', choices=MODO_CHOICES,  max_length=20, blank=True, default='facial')

    conductor = models.BooleanField('Ingreso Conductor?', blank=True, 
        default=False)

    compania = models.ForeignKey(Compania, related_name='+', verbose_name='Compañía Ingreso', blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        db_table = 'Asistencia_Detalle'
        verbose_name = 'Registro Asistencia'
        verbose_name_plural = 'Detalle Asistencia'
        ordering = ('-fecha_hora_ingreso',)

    def __str__(self):
        to_zone = timezone.get_default_timezone()
        fecha_hora_ingreso = self.fecha_hora_ingreso.astimezone(to_zone)
        fecha_hora_registro = fecha_hora_ingreso.strftime("%d-%m-%Y %H:%M:%S")
        return f"{self.personal} | {fecha_hora_registro}"

    def get_nombre_completo(self):
        return self.personal.nombre_completo

    def get_rut(self):
        return self.personal.rut
    get_rut.short_description = 'RUT'

    def get_ccia(self):
        compania = self.compania
        compania = compania if compania else self.personal.compania
        return compania
    get_ccia.short_description = 'CIA'

    def get_operador_rescate(self):
        return self.personal.get_operador_rescate_fmt()
    get_operador_rescate.short_description = 'Operador'

    def get_conductor(self):
        return 'SI' if self.conductor else 'NO'
    get_conductor.short_description = 'Conductor'

    def get_fecha_hora_ingreso(self):
        to_zone = timezone.get_default_timezone()
        dt = self.fecha_hora_ingreso.astimezone(to_zone)
        return dt.strftime("%d/%m/%Y %H:%M:%S")

    def get_fecha_hora_salida(self):
        if self.fecha_hora_salida:
            to_zone = timezone.get_default_timezone()
            dt = self.fecha_hora_salida.astimezone(to_zone)
            return dt.strftime("%d/%m/%Y %H:%M:%S")
        else:
            return '-'
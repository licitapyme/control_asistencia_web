# Generated by Django 2.0.7 on 2020-08-20 21:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0008_auto_20200820_1339'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bomberovoluntario',
            name='ccia',
        ),
        migrations.AlterField(
            model_name='asistenciadetalle',
            name='compania',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='frontend.Compania', verbose_name='Compañía Ingreso'),
        ),
    ]

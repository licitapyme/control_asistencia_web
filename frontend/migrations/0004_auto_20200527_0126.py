# Generated by Django 2.0.7 on 2020-05-27 06:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0003_auto_20200527_0114'),
    ]

    operations = [
        migrations.AlterField(
            model_name='asistenciadetalle',
            name='temperatura_ingreso',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True, verbose_name='Temperatura Ingreso'),
        ),
        migrations.AlterField(
            model_name='asistenciadetalle',
            name='temperatura_salida',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True, verbose_name='Temperatura Salida'),
        ),
    ]

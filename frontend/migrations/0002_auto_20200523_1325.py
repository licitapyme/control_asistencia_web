# Generated by Django 2.0.7 on 2020-05-23 18:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bomberovoluntario',
            name='internal_id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]

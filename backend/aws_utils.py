
import os
import boto3
from botocore.exceptions import ClientError

from django.conf import settings

import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)


# AWS Credentials and default Region 
AWS_SETTINGS = {
    "aws_access_key_id": "AKIA6C5NHN645LJ6IQPA",
    "aws_secret_access_key": "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",
    "region_name": "us-west-2",
}


def compare_images(source_path, target_path, similarity_threshold=90):
    """ Compare photo images using 'compare_faces' method (AWSrekognition) """

    # Init session
    session = boto3.Session(**AWS_SETTINGS)
    # 
    client = session.client('rekognition')

    # 
    source_file = os.path.join(settings.MEDIA_ROOT, source_path)
    target_file = target_path
    # target_file = os.path.join(settings.MEDIA_ROOT, target_path)

    similarity = 0
    try:
        # 
        image_source=open(source_file,'rb')
        image_target=open(target_file,'rb')

        response = client.compare_faces(
            SimilarityThreshold=similarity_threshold,
            SourceImage={'Bytes': image_source.read()},
            TargetImage={'Bytes': image_target.read()}
        )

        for face_match in response['FaceMatches']:
            position = face_match['Face']['BoundingBox']
            similarity = face_match['Similarity']
            logger.info('The face at %s matches with %d %% similarity.', target_path, similarity)
        # 
    except ClientError as e:
        logger.error(e)
    except Exception:
        logger.exception("Unexpected exception occurred! (compare_images)", exc_info=True)
    finally:
        image_source.close()
        image_target.close()

    return True if similarity > similarity_threshold else False


if __name__ == "__main__":
    pass
    # compare_images(...)

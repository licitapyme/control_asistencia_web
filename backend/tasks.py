# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task, task
from celery import group
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

from frontend.models import BomberoVoluntario

from backend.aws_utils import compare_images

import random
import time


def compare_faces_wrapper_task(target_path, **kwargs):

    # Split entries
    n = kwargs.get('split', 8)

    qs = BomberoVoluntario.objects.filter(status='active')
    # 
    compania = kwargs.get('compania', -1)
    if compania > 0:
        qs = qs.filter(compania=compania)
    # 
    all_records = [ obj.as_dict() for obj in qs if obj.has_picture() ]

    splits = ( all_records[i:i + n] for i in range(0, len(all_records), n) )
    # 
    internal_id = -1
    for split in splits:
        result = group(compare_faces_task.s(target_path=target_path, **kwargs) for kwargs in split).apply_async()
        results = []
        while True:
            try:
                logger.debug("Ready: {}".format(result.ready()))
                results = result.get(timeout=1.0)
                break
            except Exception:
                logger.debug("I am still alive and waiting for results...")
                pass
        if any(res['result'] for res in results):
            internal_id = [ res['internal_id'] for res in results if res['result'] ][0]
            break
        # Pacing (1-2 seconds)
        seconds = int(1 + (random.random() * 1))
        time.sleep(seconds)

    return internal_id


@shared_task
def compare_faces_task(internal_id, source_path, target_path):

    result = compare_images(source_path, target_path)

    return {
        'internal_id': internal_id,
        'result': result
    }

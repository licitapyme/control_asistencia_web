#!/bin/bash

NAME="control_asistencia_web"                                  # Name of the application
DJANGODIR=/home/django/virtualenvs/venv/control_asistencia_web	# Django project directory
SOCKFILE=/home/django/virtualenvs/venv/run/gunicorn.sock  # we will communicate using this unix socket
USER=django                                        # the user to run as
GROUP=webapps                                     # the group to run as
NUM_WORKERS=2                                     # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=control_asistencia_web.settings             # which settings file should Django use
DJANGO_WSGI_MODULE=control_asistencia_web.wsgi                     # WSGI module name

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source ../bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
export DEBUG=off

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec ../bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --bind=unix:$SOCKFILE \
  --log-level=debug \
  --log-file=-
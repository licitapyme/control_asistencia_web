#!/bin/bash

NAME="control_asistencia_celery_worker"                             # Name of the application
DJANGODIR=/home/django/virtualenvs/venv/control_asistencia_web  # Django project directory
USER=django                                       # the user to run as
GROUP=webapps                                     # the group to run as
DJANGO_SETTINGS_MODULE=control_asistencia_web.settings  # which settings file should Django use
#DJANGO_WSGI_MODULE=control_asistencia_web.wsgi          # WSGI module name
# NUM_PROC=2

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source ../bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
export DEBUG=off


# Start your Django process tasks mngmt tool
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
celery -A control_asistencia_web worker -Q normal -l info --autoscale=8,2 -n worker1@%%h

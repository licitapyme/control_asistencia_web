"""control_asistencia_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.auth.forms import \
    AuthenticationForm

from django.conf.urls import include, url
from django.urls import reverse_lazy

from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^login/$',
        auth_views.login,
        kwargs={'template_name': 'user/login.html'}, 
        name='login'),
    url(r'^logout/$', 
        auth_views.logout,
        kwargs={
            'template_name': 'user/logged_out.html',
            'extra_context': {'form': AuthenticationForm},
            # 'next_page': reverse_lazy('home')
        },
        name='logout'),

    url(r'^', include('frontend.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


from __future__ import absolute_import
import os
from celery import Celery
from celery.signals import worker_process_init

# Workaround for error message:
# ValueError: not enough values to unpack (expected 3, got '0)
os.environ.setdefault('FORKED_BY_MULTIPROCESSING', '1')

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'control_asistencia_web.settings')
app = Celery('control_asistencia_web')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@worker_process_init.connect
def fix_multiprocessing(**kwargs):
	# don't be a daemon, so we can create new subprocesses
	from multiprocessing import current_process
	current_process().daemon = False


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))

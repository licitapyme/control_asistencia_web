celery==4.2.0
Django==2.0.7
django-celery-beat==1.5.0
redis==3.3.11
mysqlclient==1.4.4
boto3==1.13.16
django-import-export==1.2.0
django-admin-rangefilter==0.6.0